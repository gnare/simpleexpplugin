package com.voxelbuster.simpleexpplugin;

import java.util.ArrayList;
import java.util.HashMap;

public class ConfigManager {

    private class Config {
        private ArrayList<String> rightClickHarvest = new ArrayList<>();

        private HashMap<String, Integer> plantedCrops = new HashMap<>();
        private HashMap<String, Integer> harvestedCrops = new HashMap<>();
        private HashMap<String, Integer> fertilizedCrops = new HashMap<>();

        private HashMap<String, Integer> itemsToExp = new HashMap<>();
    }
}
